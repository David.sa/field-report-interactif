# field report interactif
## Pésentation du projet jeu Simplon 1.0.2
Le but de ce projet est d'utilisé les competences aquise en **javascript** durant les cours, et d'en developper de nouvelle grace a l'auto-formation.

## Languages utilisées
* **HTML**
* **CSS**
* **JAVASCRIPT**
    * **queryselector**
    * **addeventlistener**.

## Interet
1. Mise en pratique des connaissances

2. utilisation et apprentissage du **javascript**

## Modifications
le premiere modification concernera:
* Un deuxieme bouton pour faire revenir le texte en arriere

La seconde modifications a été:
* rendre plus visible le bouton de defilement du texte

## Exemple de code utilisé

Sélection de mon code **HTML** en **javascript**

```
let box = document.querySelector('#boxb');
let def = document.querySelector('.def');
let def2 = document.querySelector('.def2');
let photo = document.querySelector('#photo');
let titre = document.querySelector('#titre');
let chapitre = document.querySelector('#chap');
let choix = document.querySelector('.choix');
```
Liste des actions a éffectuez a l'évenement "click"

```
box.addEventListener('click', function(){
    titre.remove();
    chapitre.remove();
    def.remove();
    def2.remove();
    choix.remove();
    btn.remove();
    para.textContent = 'Vous n\'avez rien répondu, elle est repartie avec ses amies'
    para.style.fontSize = "30px";
    photo.src = './img/game-over.png';
    photo.style.width = '600px';
})
```
## Diagramme Use Case

![20% center](./img/UseCaseDiagram1.png)

## Jeux
voila la page d'acceuille qui presente le concept du jeu

![20% center](./img/presentationjeux.png)

et ici comment se presente le jeux

![20% center](./img/presentationjeux2.png)

## Difficultés

Je n'ai pas eu de difficulté parctiiculière, mais c'etait projet relativement facile.

## Copyright

©2018-2019 DavidSalvador All right reserved.

(pour voir mes autre création rendez-vous sur [Gitlab](https://gitlab.com/dashboard/projects).)